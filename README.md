# Crucible 正體中文（以及配合BABEL翻譯）
本系統的BABEL支援並不完善，所以天賦樹及技能部分採用合集包替換的方式，中文化時需將原系統的以下合集包。

- System Rules
- Talents

替換為本MOD的內容。